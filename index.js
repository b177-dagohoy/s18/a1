let pokeTrainer = {
	name: 'Misty',
	age: 18,
	pokemon: ['Snorlax', 'Vulpix', 'Psyduck', 'Togepi', 'Jigglypuff'],
	friends: ['Ash', 'Brock'],
	talk: function(){
		console.log('Snorlax! I choose you!')
	}
}

console.log(pokeTrainer);

console.log("Result of dot notation:");
console.log(pokeTrainer.name);

console.log("Result of square bracket notation:");
console.log(pokeTrainer['pokemon']);

console.log("Result of talk notation:");
pokeTrainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		reducedHealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (reducedHealth <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

let snorlax = new Pokemon('Snorlax', 50);
let vulpix = new Pokemon('Vulpix', 30);
let psyduck = new Pokemon('Psyduck', 50);
let togepi = new Pokemon('Togepi', 40);
let jigglypuff = new Pokemon('Jigglypuff', 20);

console.log(snorlax);
console.log(vulpix);
console.log(psyduck);
console.log(togepi);
console.log(jigglypuff);

snorlax.tackle(vulpix);
vulpix.tackle(psyduck);
togepi.tackle(jigglypuff);

